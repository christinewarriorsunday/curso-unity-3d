﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Ingresar el sueldo de una persona, si supera los 3000 pesos mostrar un mensaje en pantalla indicando que debe abonar impuestos.

namespace EstructuraCondicionalSimple1
{
    class CondicionalSimples
    {
        static void Main(string[] args)
        {
            //Declaración de variable con decimales.
            float sueldo;
            //Declaración de variable de cadena de caracteres.
            string linea;
            //Solicitamos el sueldo.
            Console.Write("Ingrese el sueldo:");
            //Introducimos el dato en la variable string.
            linea = Console.ReadLine();
            //Convertimos la variable string en variable decimal y la introducimos en la variable sueldo de tipo float
            sueldo = float.Parse(linea);
            //Empezamos el condicional.
            // Preguntamos si el valor de sueldo es mayor de 3000.
            if (sueldo > 3000)
            //En caso que se cumpla la condición entra en el programa. Si no se cumple, ignora lo de dentro de las llaves y continua el programa.
            {
                //Entonces escribe el texto que le hemos indicado.
                Console.Write("Esta persona debe abonar impuestos");
            }
            Console.ReadKey();
        }
    }
}
/*Ejercicios:
 * 1.- Se ingresan tres notas de un alumno, si el promedio es mayor o igual a siete mostrar un mensaje "Promocionado".

  
 
 
 
 








 
 
 
 
 
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraCondicionalSimple2
{
    class Program
    {
        static void Main(string[] args)
        {
    	    int nota1,nota2,nota3;
            string linea;
    	    Console.Write("Ingrese primer nota:");
            linea=Console.ReadLine();
    	    nota1=int.Parse(linea);
    	    Console.Write("Ingrese segunda nota:");
            linea=Console.ReadLine();
    	    nota2=int.Parse(linea);
    	    Console.Write("Ingrese tercer nota:");
            linea=Console.ReadLine();
    	    nota3=int.Parse(linea);
            int promedio;
            promedio=(nota1 + nota2 + nota3) / 3;
    	    if (promedio>=7) 
            {
    	        Console.Write("Promocionado");
    	    }
            Console.ReadKey();
        }
    }
}

 */