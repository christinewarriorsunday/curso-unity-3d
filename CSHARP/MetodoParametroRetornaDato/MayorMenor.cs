﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetodoParametroRetornaDato
{
    //Confeccionar una clase que permita ingresar tres valores por teclado. Luego mostrar el mayor y el menor.
    class MayorMenor
    {
        //Método para pedir los valores al usuario, es void porque no devuelve datos.
        public void cargarValores()
        {
            //Declaramos variable
            string linea;
            Console.Write("Ingrese primer valor:");
            linea = Console.ReadLine();
            //declaramos la variable y a la vez le introducimos el valor convertido.
            int valor1 = int.Parse(linea);
            Console.Write("Ingrese segundo valor:");
            linea = Console.ReadLine();
            int valor2 = int.Parse(linea);
            Console.Write("Ingrese tercer valor:");
            linea = Console.ReadLine();
            int valor3 = int.Parse(linea);
            //Declaramos dos variables donde vamos a meter el valor que nos va a devolver cada método.
            int mayor, menor;
            //Llamamos al primer método enviándole los tres datos y esperando que nos devuelva uno que lo introducimos en la variable.
            mayor = CalcularMayor(valor1, valor2, valor3);
            //Llamamos al segundo método enviándole los tres datos y esperando que nos devuelva uno que lo introducimos en la variable.
            menor = CalcularMenor(valor1, valor2, valor3);
            //Mostramos los datos que hemos recibido.
            Console.WriteLine("El valor mayor de los tres es:" + mayor);
            Console.WriteLine("El valor menor de los tres es:" + menor);
        }

        //Método que calcula el mayor, recibe 3 datos y se declara int porque maneja datos enteros y va a devolver un dato entero.
        
        public int CalcularMayor(int v1, int v2, int v3)
        {
            //Declaramos variable.
            int devolver;
            //Preguntamos
            if (v1 > v2 && v1 > v3)
            {
                devolver = v1;
            }
            else
            {
                if (v2 > v3)
                {
                    devolver = v2;
                }
                else
                {
                    devolver = v3;
                }
            }
            //Y el dato válido, es el que devolvemos y mostraremos como mayor.
            return devolver;
        }
        //Método que calcula el mayor, recibe 3 datos y se declara int porque maneja datos enteros y va a devolver un dato entero.
        public int CalcularMenor(int v1, int v2, int v3)
        {
            //Declaramos variable.
            int m;
            //Preguntamos
            if (v1 < v2 && v1 < v3)
            {
                m = v1;
            }
            else
            {
                if (v2 < v3)
                {
                    m = v2;
                }
                else
                {
                    m = v3;
                }
            }
            //Y el dato válido, es el que devolvemos y mostraremos como menor.
            return m;
        }
        //Método principal de inicio
        static void Main(string[] args)
        {
            //Declaramos un nuevo método para poder llamar al método que necesitamos.
            MayorMenor mm = new MayorMenor();
            //Lo llamamos
            mm.cargarValores();
            //Terminamos el programa.
            Console.ReadKey();
        }
    }
}
