﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConversionDatos
{
	public class ConversionesDatos
	{
		public static void Main()
		{
			// Convertir número en texto y float en double
			// Conversiones implícitas:
			//Variable entero
			int edad = 40;
			//Variable caracter de texto.
			string resultado;
			//Conversión implícita de entero a texto.
			resultado = "" + edad;
			resultado += " era un número y ahora es texto";
			Console.WriteLine(resultado);
			
			//Variable decimal
			double numDecimal = 6.4545f;
			//Conversión implícita de decimal a texto.
			resultado = resultado + ", el numDecimal = " + numDecimal;
			Console.WriteLine(resultado);

			// Conversiones explícitas, se pone entre paréntesis el tipo:
			float otroDecimal = (float)1.23456733123;
			resultado = resultado + ", otroDecimal = " + otroDecimal;
			Console.WriteLine(resultado);

			// Conversiones complejas: de texto a número
			int unEntero = int.Parse("3434");
			resultado = resultado + ", unEntero = " + unEntero;
			Console.WriteLine(resultado);
			Console.ReadKey();
		}
	}
}
