﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba1
{
    //Definir un array de 5 componentes de tipo float que representen las alturas de 5 personas. Obtener el promedio de las mismas. Contar cuántas personas son más altas que el promedio y cuántas más bajas.
    class AltosyBajos
    {
        private float[] promedioAlt;

        private float promedio;
        static void Main(string[] args)
        {
            AltosyBajos AltBaj = new AltosyBajos();
            AltBaj.Cargar();
            AltBaj.CalcularPromedio();
            AltBaj.AltosBajos();
        }

        private void AltosBajos()
        {
            int Alt = 0, Baj = 0;

            for (int f = 0; f<5; f++)
            {
                if (promedioAlt[f] > promedio)
                {
                    Alt++;
                }
                else
                {
                    if (promedioAlt[f] < promedio)
                    {
                        Baj++;
                    }
                }
            }
            Console.WriteLine("Cantidad de personas más altas al promedio son: " + Alt);
            Console.WriteLine("Cantidad de personas más bajas al promedio son: " + Baj);
            Console.ReadKey();
        }

        private void CalcularPromedio()
        {
            float suma = 0;
            
            for (int f = 0; f < 5; f++)
            {
                suma += promedioAlt[f];
            }
            promedio = suma / 5;

            Console.WriteLine("El promedio de las 5 alturas es: " + promedio);
        }

        private void Cargar()
        {
            promedioAlt = new float[5];

            for (int f = 0; f < 5; f++)
            {
                Console.Write("Ingrese la altura de la persona " + (f + 1) + " : ");
                string linea = Console.ReadLine();
                //Rellenar el array con el dato que corresponde.
                promedioAlt[f] = float.Parse(linea);
            }

        }
    }
}
