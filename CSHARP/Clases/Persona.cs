﻿/*using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clases
{
    //Confeccionar una clase que permita carga el nombre y la edad de una persona. Mostrar los datos cargados. Imprimir un mensaje si es mayor de edad (edad>=18)
    class Persona
    {
        /*Una clase es un molde del que luego se pueden crear múltiples objetos, con similares características.
        Una clase es una plantilla (molde), que define atributos (variables) y métodos (funciones)
        La clase define los atributos y métodos comunes a los objetos de ese tipo, pero luego, cada objeto tendrá sus propios valores y compartirán las mismas funciones.
        Debemos crear una clase antes de poder crear objetos (instancias) de esa clase. Al crear un objeto de una clase, se dice que se crea una instancia de la clase o un objeto propiamente dicho.*/

        //Confeccionar una clase que permita carga el nombre y la edad de una persona. Mostrar los datos cargados. Imprimir un mensaje si es mayor de edad (edad>=18)

        //Declaramos las variable externas a unas clases, pero dentro de Clases, que es donde van a funcionar y las declaramos private, para que desde fuera de Clases, nadie tenga acceso al dato de las variables fuera, para que no pueda modificar nuestro resultado.
 /*      private string nombre;
        private int edad;


        static void Main(string[] args)
        {
            //Se declara un objeto del tipo Persona y le asignamos un nombre para utilizarlo más facilmente. Y le asignamos un valor nuevo para poder utilizarlo como clase general que llama a sus clases interiores.
            Persona per1 = new Persona();
            //Llamamos a la primera función que cogerá los datos que introduzca el usuario.
            per1.Inicializar();
            //Llamamos a la segunda función que muestra el nombre y la edad.
            per1.Imprimir();
            //Llamamos a la tercera función que nos compara la edad con 18 y nos dice si es mayor de edad y termina el programa.
            per1.EsMayorEdad();
        }


        //Hacemos la primera Función, es pública para poder llamarla desde la principal "Main"
        public void Inicializar()
        {
            //Entramos en la función que carga los datos.
            //solicitamos el primer nombre al usuario.
            Console.Write("Ingrese el nombre:");
            //Lo introducimos en la variable de tipo string exterior que se queda con el dato al salir de la función.
            nombre = Console.ReadLine();
            //Declaramos la variable línea de tipo string para recoger el dato de la edad y poder luego convertirlo, lo declaramos dentro de la función para que no se confunda con otras variables y solo se use en esta función y cuando salgamos de la función, pierda su valor.
            string linea;
            //solicitamos al usuario la edad.
            Console.Write("Ingrese la edad:");
            //La introducimos en la variable linea.
            linea = Console.ReadLine();
            //La convertimos a entero y la introducimos en la variable externa a la función, que se queda con el dato.
            edad = int.Parse(linea);
            //Terminamos la función y salimos con nuestros datos cargados.
        }

        public void Imprimir()
        {
            //Entramos en la función de mostrar los datos.
            //Imprimimos lo que es.
            Console.Write("Nombre: ");
            //A continuación el dato de la variable y pasamos a la línea siguiente.
            Console.WriteLine(nombre);
            //Imprimimos lo que es.
            Console.Write("Edad:");
            //A continuación el dato de la variable y pasamos a la línea siguiente.
            Console.WriteLine(edad);
            //Terminamos la función y hemos mostrado nuestros datos cargados.
        }

        public void EsMayorEdad()
        {
            //Entramos en la función para mostrar si es mayor de 18.
            //Comparamos la edad con 18.
            if (edad >= 18)
            {
                //Si es mayor entra aquí y muestra el mensaje.
                Console.Write("Es mayor de edad");
            }
            //Si no es mayor o igual que 18 entra aquí.
            else
            {
                //Muestra el mensaje.
                Console.Write("No es mayor de edad");
            }
            //Se queda esperando a que le demos a una tecla para cerrar todo.
            Console.ReadKey();
        }


       
    }
}*/
/*
 Desarrollar un programa que cargue los lados de un triángulo e implemente los siguientes métodos: inicializar los atributos, imprimir el valor del lado mayor y otro método que muestre si es equilátero o no.








*/
 using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaClase2
{
    class Triangulo
    {
        private int lado1, lado2, lado3;

        public void Inicializar()
        {
            string linea;
            Console.Write("Medida lado 1:");
            linea = Console.ReadLine();
            lado1 = int.Parse(linea);
            Console.Write("Medida lado 2:");
            linea = Console.ReadLine();
            lado2 = int.Parse(linea);
            Console.Write("Medida lado 3:");
            linea = Console.ReadLine();
            lado3 = int.Parse(linea);
        }

        public void LadoMayor()
        {
            Console.Write("Lado mayor:");
            if (lado1 > lado2 && lado1 > lado3)
            {
                Console.WriteLine(lado1);
            }
            else
            {
                if (lado2 > lado3)
                {
                    Console.WriteLine(lado2);
                }
                else
                {
                    Console.WriteLine(lado3);
                }
            }
        }

        public void EsEquilatero() 
        {
            if (lado1==lado2 && lado1==lado3) 
            {
                Console.Write("Es un triángulo equilátero");
            }
            else 
            {
                Console.Write("No es un triángulo equilátero");            
            }
        }
    
        static void Main(string[] args)
        {
            Triangulo triangulo1 = new Triangulo();
            triangulo1.Inicializar();
            triangulo1.LadoMayor();
            triangulo1.EsEquilatero();
            Console.ReadKey();
        }
    }
}
/* 


Desarrollar una clase que represente un punto en el plano y tenga los siguientes métodos: cargar los valores de x e y, imprimir en que cuadrante se encuentra dicho punto (concepto matemático, primer cuadrante si x e y son positivas, si x<0 e y>0 segundo cuadrante, etc.)



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaClase3
{
    class Punto
    {
        private int x, y;

        public void Inicializar() 
        {
            string linea;
            Console.Write("Ingrese coordenada x :");
            linea = Console.ReadLine();
            x = int.Parse(linea);
            Console.Write("Ingrese coordenada y :");
            linea = Console.ReadLine();
            y = int.Parse(linea);
        }

        void ImprimirCuadrante() 
        {
            if (x>0 && y>0) 
            {
                Console.Write("Se encuentra en el primer cuadrante.");
            }
            else 
            {
                if (x<0 && y>0)
                {
                    Console.Write("Se encuentra en el segundo cuadrante.");
                }
                else
                {
                    if (x<0 && y<0) 
                    {
                        Console.Write("Se encuentra en el tercer cuadrante.");
                    }
                    else
                    {
                        if (x>0 && y<0) 
                        {
                            Console.Write("Se encuentra en el cuarto cuadrante.");
                        }
                        else 
                        {
                            Console.Write("El punto no está en un cuadrante.");
                        }
                    }
                }
            }
            Console.ReadKey();
        }
    
        static void Main(string[] args)
        {
            Punto punto1 = new Punto();
            punto1.Inicializar();
            punto1.ImprimirCuadrante();
        }
    }
}




Desarrollar una clase que represente un Cuadrado y tenga los siguientes métodos: cargar el valor de su lado, imprimir su perímetro y su superficie.





using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaClase4
{
    class Cuadrado
    {
        private int lado;

        public void Inicializar()
        {
            Console.Write("Ingrese el valor del lado:");
            string linea;
            linea = Console.ReadLine();
            lado=int.Parse(linea);
        }

        public void ImprimirPerimetro()
        {
            int perimetro;
            perimetro=lado*4;
            Console.WriteLine("El perímetro es:"+perimetro);
        }

        public void ImprimirSuperficie()
        {
            int superficie;
            superficie=lado*lado;
            Console.WriteLine("La superficie es:"+superficie);
        }

        static void Main(string[] args)
        {
            Cuadrado cuadrado1 = new Cuadrado();
            cuadrado1.Inicializar();
            cuadrado1.ImprimirPerimetro();
            cuadrado1.ImprimirSuperficie();
            Console.ReadKey();
        }
    }
}



Confeccionar una clase que represente un empleado. Definir como atributos su nombre y su sueldo. Confeccionar los métodos para la carga, otro para imprimir sus datos y por último uno que imprima un mensaje si debe pagar impuestos (si el sueldo supera a 3000) 






using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaClase5
{
    class Empleado
    {
        string nombre;
        float sueldo;

        public void Inicializar() 
        {
            string linea;
            Console.Write("Ingrese el nombre del empleado:");
            nombre = Console.ReadLine();
            Console.Write("Ingrese su sueldo:");
            linea = Console.ReadLine();
            sueldo=float.Parse(linea);
        }

        public void PagaImpuestos() 
        {
            if (sueldo>3000) 
            {
                Console.WriteLine("Debe abonar impuestos");
            } 
            else
            {
                Console.WriteLine("No paga impuestos");
            }
            Console.ReadKey();
        }

        static void Main(string[] args)
        {
            Empleado empleado1= new Empleado();
            empleado1.Inicializar();
            empleado1.PagaImpuestos();
        }
    }
}





Implementar la clase operaciones. Se deben cargar dos valores enteros, calcular su suma, resta, multiplicación y división, cada una en un método, imprimir dichos resultados. 




using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaClase5
{
    class Operaciones
    {
        private int valor1, valor2;

        public void Inicializar() 
        {
            string linea;
            Console.Write("Ingrese primer valor:");
            linea=Console.ReadLine();
            valor1=int.Parse(linea);
            Console.Write("Ingrese segundo valor:");
            linea = Console.ReadLine();
            valor2=int.Parse(linea);
        }

        public void Sumar() 
        {
            int suma;
            suma=valor1+valor2;
            Console.WriteLine("La suma es:"+suma);
        }

        public void Restar() 
        {
            int resta;
            resta=valor1-valor2;
            Console.WriteLine("La resta es:"+resta);        
        }

        public void Multiplicar() 
        {
            int multiplicacion;
            multiplicacion=valor1*valor2;
            Console.WriteLine("La multiplicación es:"+multiplicacion);
        }

        public void Dividir() 
        {
            int division;
            division = valor1 / valor2;
            Console.WriteLine("La división es:" + division);
        }
    

        static void Main(string[] args)
        {
            Operaciones operacion1 = new Operaciones();
            operacion1.Inicializar();
            operacion1.Sumar();
            operacion1.Restar();
            operacion1.Multiplicar();
            operacion1.Dividir();
            Console.ReadKey();
        }
    }
}
 */