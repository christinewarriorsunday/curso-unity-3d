﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaVector8
{
    //Se desea almacenar los sueldos de operarios. Cuando se ejecuta el programa se debe pedir la cantidad de sueldos a ingresar. Luego crear un array con dicho tamaño.
    class PruebaVector8
    {
        //Creamos el Array, le damos nombre y tipo
        private int[] sueldos;

        public void Cargar()
        {
            //Preguntamos por el número de celdas que va a tener el array.
            Console.Write("Cuantos sueldos cargará:");
            string linea;
            linea = Console.ReadLine();
            //Lo cargamos en la variable entera
            int cant = int.Parse(linea);
            //Inicializamos el array, le damos el nombre y el tamaño.
            sueldos = new int[cant];
            //Le decimos que el array(sueldos).Length(tamaño) y que tiene que sumar 1.
            for (int f = 0; f < sueldos.Length; f++)
            {
                //Pedimos el dato
                Console.Write("Ingrese sueldo:");
                linea = Console.ReadLine();
                //Lo introducimos
                //Utilizamos la variable del for "f" inicializada en "0"
                sueldos[f] = int.Parse(linea);
                /*Utilizamos el tamaño del array, por eso hay que descontarle "1" para que empiece desde "0"
                sueldos[sueldos.Length-1] = int.Parse(linea);*/
            }
        }

        public void Imprimir()
        {
            for (int f = 0; f < sueldos.Length; f++)
            {
                Console.WriteLine(sueldos[f]);
            }
            Console.ReadKey();
        }

        static void Main(string[] args)
        {
            //Crear el objeto de la clase, darle nombre para poder llamar a las distintas subclases.
            PruebaVector8 pv = new PruebaVector8();

            pv.Cargar();
            pv.Imprimir();
        }
    }
}

/*
//Desarrollar un programa que permita cargar 5 nombres de personas y sus edades respectivas. Luego de realizar la carga por teclado de todos los datos imprimir los nombres de las personas mayores de edad (mayores o iguales a 18 años)


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaVector10
{
    class PruebaVector10
    {
        //Declaramos dos Arrays paralelos, uno para los nombres y otro para las edades.
        //El de nombre string.
        private string[] nombres;
        //El de edades de tipo entero.
        private int[] edades;

        //Creamos el Main
        static void Main(string[] args)
        {
            //Creamos el objeto de la clase principal para poder acceder a las subclases.
            PruebaVector10 pv = new PruebaVector10();
            //Llamamos a las subclases para realizar el programa.
            pv.Cargar();
            pv.MayoresEdad();

        }

        //Creamos la clase para saber si son mayores de edad
        private void MayoresEdad()
        {
            //Informamos que les vamos a mostrar las personas mayores de edad.
            Console.WriteLine("Personas mayores de edad.");
            //Iniciamos el for y para saber cuantas veces debe repetir el bucle utilizamos la longitud del array, en este caso da igual cual cojamos porque los dos son paralelos y tienen la misma longitud.
            for (int f = 0; f < nombres.Length; f++)
            {
                //Preguntamos por la edad si es mayor o igual que 18.
                if(edades[f] >= 18)
                {
                    //si es mayor o igual, entramos y lo mostramos
                    Console.WriteLine(nombres[f]);
                }
            }
            //Fin del programa, en espera de que se presione una tecla para cerrar.
            Console.ReadKey();
        }

        //Creamos la clase para cargar los datos que le pedimos al usuario.
        private void Cargar()
        {
            //Inicializamos los dos arrays diciéndole que lo cree del tipo que va a ser y el tamaño.
            nombres = new string[5];
            edades = new int[5];
            //Iniciamos el bucle con el for para ir preguntando por el nombre y la edad. También utilizamos la longitud del array nombres como número de ciclos del bucle.
            for(int f=0; f < nombres.Length; f++)
            {
                //Pedimos que introduzca el nombre.
                Console.Write("Ingrese nombre: ");
                //Lo introducimos directamente en el array porque es de tipo string.
                nombres[f] = Console.ReadLine();
                //Pedimos que introduzca la edad.
                Console.Write("Ingrese edad: ");
                //Creamos la variable string para recoger el dato que ingresa el usuario.
                string linea;
                //Lo introducimos en la variable.
                linea = Console.ReadLine();
                //Lo convertimos en entero y lo introducimos en el array de edades.
                edades[f] = int.Parse(linea);
            }
        }
    }
}




/*Problema 1:

Confeccionar un programa que permita cargar los nombres de 5 operarios y sus sueldos respectivos. Mostrar el sueldo mayor y el nombre del operario.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaVector11
{
    class PruebaVector11
    {
        private string[] nombres;
        private float[] sueldos;

        public void Cargar() 
        {
            nombres=new string[5];
            sueldos=new float[5];
            for(int f=0;f < nombres.Length;f++) 
            {
                Console.Write("Ingrese el nombre del empleado:");
                nombres[f] = Console.ReadLine();
                Console.Write("Ingrese el sueldo:");
                string linea;
                linea = Console.ReadLine();
                sueldos[f]=float.Parse(linea);
            }
        }

        public void MayorSueldo() 
        {
            float mayor;
            int pos;
            mayor=sueldos[0];
            pos=0;
            for(int f=1;f < nombres.Length;f++) 
            {
                if (sueldos[f] > mayor) 
                {
                    mayor=sueldos[f];
                    pos=f;
                }
            }
            Console.WriteLine("El empleado con sueldo mayor es "+nombres[pos]);
            Console.WriteLine("Tiene un sueldo:"+mayor);
            Console.ReadKey();
        }

        static void Main(string[] args)
        {
            PruebaVector11 pv = new PruebaVector11();
            pv.Cargar();
            pv.MayorSueldo();
        }
    }
}


Problema 2
Cargar un vector de n elementos. imprimir el menor y un mensaje si se repite dentro del vector. 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaVector12
{
    class PruebaVector12
    {
        private int[] vec;
        private int menor;

        public void Cargar() 
        {
            Console.Write("Cuantos elementos desea cargar:");
            string linea;
            linea = Console.ReadLine();
            int n=int.Parse(linea);
            vec=new int[n];
            for(int f=0;f < vec.Length;f++) 
            {
                Console.Write("Ingrese número:");
                linea = Console.ReadLine();
                vec[f]=int.Parse(linea);
            }
        }

        public void MenorElemento() 
        {
            menor=vec[0];
            for(int f=1;f < vec.Length;f++) 
            {
                if (vec[f] < menor) 
                {
                    menor=vec[f];
                }
            }
            Console.WriteLine("El elemento menor es:"+menor);        
        }

        public void RepiteMenor() 
        {
            int cant=0;
            for(int f=0;f < vec.Length;f++) 
            {
                if (vec[f]==menor) 
                {
                    cant++;
                }
            }
            if (cant > 1) 
            {
                Console.WriteLine("Se repite el menor en el vector.");    
            }
            else 
            {
                Console.WriteLine("No se repite el menor en el vector.");
            }
            Console.ReadLine();
        }

        static void Main(string[] args)
        {
            PruebaVector12 pv = new PruebaVector12();
            pv.Cargar();
            pv.MenorElemento();
            pv.RepiteMenor();
        }
    }
}

*/