﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadenasDeCaracteres
{
    //Solicitar el ingreso del nombre y edad de dos personas. Mostrar el nombre de la persona con mayor edad.
    class CadenasDeCaracteres
    {
        static void Main(string[] args)
        {
            //Declaramos las variables de tipo cadena de caracteres para introducir los nombres.
            String nombre1, nombre2;
            //Declaramos las variables de número para las edades.
            int edad1, edad2;
            //Declaramos la variable para recoger los datos que introduce el usuario.
            String linea;
            //Solicitamos el primer nombre.
            Console.Write("Ingrese el nombre:");
            //Lo introducimos en la primera variable de caracteres.
            nombre1 = Console.ReadLine();
            //Pedimos la edad.
            Console.Write("Ingrese edad:");
            //La introducimos en la variable de caracteres que luego convertimos.
            linea = Console.ReadLine();
            //Convertimos la cadena de caracteres en número y la introducimos en la variable de edad.
            edad1 = int.Parse(linea);
            //Pedimos el segundo nombre.
            Console.Write("Ingrese el nombre:");
            nombre2 = Console.ReadLine();
            //Pedimos la edad del segundo.
            Console.Write("Ingrese edad:");
            linea = Console.ReadLine();
            edad2 = int.Parse(linea);
            
            
            /*
             * float uno = 0.23f;
             * _ = $"Texto aleatorio: {edad1},{edad2},{uno}";
             * conversión implícita de dos variables int o cualquiera, uno es float, en texto.
             * 
             */


            //Escribimos la solución, primero el enunciado.
            Console.Write("La persona de mayor edad es:");
            //Preguntamos si la primera edad es mayor que la segunda.
            if (edad1 > edad2)
            {
                //Si es el primero lo mostramos.
                Console.Write(nombre1);
            }
            //si es el segundo entramos en el else
            else
            {
                //Escribimos el segundo.
                Console.Write(nombre2);
            }
            Console.ReadKey();
        }
    }
}
/*
 Solicitar el ingreso de dos apellidos. Mostrar un mensaje si son iguales o distintos.
----Para comparar si el contenido de dos string son iguales se utiliza el operador == como si se estuvieran comparando dos enteros.



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadenaDeCaracteres3
{
    class Program
    {
        static void Main(string[] args)
        {
            string apellido1,apellido2;
            Console.Write("Ingrese primer apellido:");
            apellido1=Console.ReadLine();
            Console.Write("Ingrese segundo apellido:");
            apellido2=Console.ReadLine();
            if (apellido1==apellido2)
            {
                Console.Write("Los apellidos son iguales");
            }
            else
            {
                Console.Write("Los apellidos son distintos");
            }
            Console.ReadKey();
        }
    }
}



 
 
*/
