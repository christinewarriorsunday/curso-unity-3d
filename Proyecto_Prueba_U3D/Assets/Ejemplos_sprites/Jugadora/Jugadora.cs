﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jugadora : MonoBehaviour
{
    public GameObject spriteJug;
    Vector3 velocidad;
    public float gravedad = -9.8f;

    // Start is called before the first frame update
    void Start()
    {
        
    }
    const float VELOCIDAD_X = 10;

    // Update is called once per frame
    void Update() {
        velocidad.x = 0;

        if (Input.GetKey(KeyCode.LeftArrow) == true) {
            velocidad.x = -VELOCIDAD_X;
        } 
        if (Input.GetKey(KeyCode.RightArrow) == true) {
            velocidad.x = +VELOCIDAD_X;
        }
        if (Input.GetKey(KeyCode.Space) && transform.position.y <= 0) {
            velocidad.y = 15;
        }
        velocidad.y += gravedad * Time.deltaTime;

        transform.position += velocidad * Time.deltaTime;

        spriteJug.GetComponent<Animator>().SetFloat("velocidad_x", Mathf.Abs(velocidad.x));
        if (velocidad.x > 0)
            spriteJug.transform.localScale = Vector3.one;
        if (velocidad.x < 0)
            spriteJug.transform.localScale = new Vector3(-1, 1, 1);

        if (transform.position.y < 0) {
            gravedad = 0;
            velocidad.y = 0;
        }
        else {
            gravedad = -30f;
        }
        /*if (velocidad.x < 0 || velocidad.x > 0)   Alternativa
            spriteJug.GetComponent<Animator>().SetFloat("velocidad_x", 1);*/
    }

}
